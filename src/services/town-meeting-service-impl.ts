import { Meeting } from "../entities";
import TownMeetingService from "./town-meeting-service";
import { TownMeetingDaoPostgres } from "../daos/town-meeting-dao-postgres";
import { TownMeetingDAO } from "../daos/town-meeting-dao";


export class TownMeetingServicesImpl implements TownMeetingService
{
    townmeetingDAO:TownMeetingDAO = new TownMeetingDaoPostgres();

    retrieveAllMeetings(): Promise<Meeting[]>
    {
        return this.townmeetingDAO.getAllMeetings();
    }

    retrieveMeetingByID(meeting_id:number): Promise<Meeting>
    {
        return this.townmeetingDAO.getMeetingByID(meeting_id)
    }

    removeMeetingByID(meeting_id:number): Promise<boolean>
    {
        return this.townmeetingDAO.deleteMeetingByID(meeting_id)
    }

    createMeeting(meeting: Meeting): Promise<Meeting>
    {
        return this.townmeetingDAO.createMeeting(meeting)
   
    }

    async updateMeetingByID(meeting: Meeting):Promise<Meeting>{
        const testMeetingByID:Meeting = await this.townmeetingDAO.getMeetingByID(meeting.meeting_id);
        if(testMeetingByID.meeting_id != meeting.meeting_id){
            //throw Error(`Meeting IDs does not match`);

            
        }        
        return this.townmeetingDAO.updateMeetingByID(meeting)
    }
}