import { Meeting } from "../entities";

export default interface TownMeetingService{

    retrieveAllMeetings(): Promise<Meeting[]>;

    retrieveMeetingByID(meeting_id:number): Promise<Meeting>;

    removeMeetingByID(meeting_id:number): Promise<boolean>

    createMeeting(meeting:Meeting):Promise<Meeting>;

    updateMeetingByID(meeting_id:Meeting):Promise<Meeting>
}