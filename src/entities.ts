export class Meeting{
    constructor(
        public meeting_id: number,
        public meeting_location: string,
        public meeting_time: string,
        public meeting_topic: string
    )
    {}
}