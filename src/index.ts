import express from 'express'
import cors from 'cors'
import {InvalidCredentials, MissingResourceError} from './errors'
import {Meeting} from './entities'
import TownMeetingService from './services/town-meeting-service';
import {TownMeetingServicesImpl} from './services/town-meeting-service-impl'

const bunyan = require('bunyan');
const {LoggingBunyan} = require('@google-cloud/logging-bunyan')
const cloudLogger = new LoggingBunyan()
// Needed to test locally

//     {
//     projectId: 'town-issue-review-board-p2',
//     keyFilename: '/Users/j.esquivel.gutierrez/Downloads/town-issue-review-board-p2-2b5338a42427.json'
// });


const config = {
    name:'town-meeting-logs',
    //streams are where you log the data
    streams:[
        cloudLogger.stream('info')
    ]
}

const logger = bunyan.createLogger(config);

const app = express()

app.use(express.json())
app.use(cors())

const townmeetingService:TownMeetingService = new TownMeetingServicesImpl();

app.get('/meetings', async(req,res)=>{
    
    try 
    {
        const meetings: Meeting[] = await townmeetingService.retrieveAllMeetings();
        logger.info("Got meetings")
        res.status(200);
        res.send(meetings)
    } 
    catch (error)
    {
        if(error instanceof MissingResourceError)
        {
            res.status(404)
            res.send(error)
        }
    }
})

app.get('/meetings/:id', async(req,res)=>{
    
    try
    {
        const meetingID = Number(req.params.id)
        const meeting:Meeting = await townmeetingService.retrieveMeetingByID(meetingID);
        res.send(meeting)
    }
    catch (error) 
    {
        if(error instanceof MissingResourceError)
        {
            res.send(error)
        }
    }
})



// POST
app.post('/meetings', async (req,res)=>{

    try
    {
        const pw = req.headers.authorization;
        if(pw === "TheClassic")
        {
            let meeting:Meeting = req.body;
            meeting = await townmeetingService.createMeeting(meeting);
            logger.info("Created a Meeting")
            res.status(200);
            res.send(meeting);
        }
        else
        {
            throw new InvalidCredentials("Not Authorized to POST")
        }
    }
    catch (error)
    {
        res.send(error)
    }

})
// PUTme
app.put('/meetings/:id', async (req,res)=>{

    try
    {
        const pw = req.headers.authorization;
        if(pw === "TheClassic")
        {
            const meetingID:number = Number (req.params.id);
            const meeting:Meeting = req.body
            meeting.meeting_id = meetingID
           
            const updatemeeting = await townmeetingService.updateMeetingByID(meeting);
            logger.info("Modified a Meeting")
            res.send(updatemeeting);
        }
        else
        {
            throw new InvalidCredentials("Not Authorized to PUT")
        }
    } 
    catch (error)
    {
        res.send(error)
    }

})
// Delete

app.delete('/meetings/:id', async(req,res)=>{

    try
    {
        const pw = req.headers.authorization; // lowercase A for authorization even though in postman I made it "Authorization
        if(pw === "TheClassic")
        {
            const meetingID = Number(req.params.id)
            const meeting:boolean = await townmeetingService.removeMeetingByID(meetingID);
            logger.info("Deleted a Meeting")
            res.send(meeting)
        }
        else
        {
            throw new InvalidCredentials("Not Authorized to DELETE")
        }
    } 
    catch (error)
    {
        res.send(error)
    }
})


const PORT = process.env.PORT || 3000;
app.listen(PORT,()=>{console.log("Application Started")})