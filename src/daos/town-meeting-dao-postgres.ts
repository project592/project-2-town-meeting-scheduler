import { Meeting } from "../entities";
import { connection } from "../connection";
import { MissingResourceError } from "../errors";
import { TownMeetingDAO } from "./town-meeting-dao";

export class TownMeetingDaoPostgres implements TownMeetingDAO
{
    async getAllMeetings(): Promise<Meeting[]>
    {
        const sql:string = "select * from meeting"; 
        const result = await connection.query(sql);
        const meetings: Meeting[] = []
        for(const row of result.rows)
        {
            const meeting:Meeting = new Meeting(row.meeting_id, row.meeting_location, row.meeting_time, row.meeting_topic)
            meetings.push(meeting)
        }
        if(meetings.length === 0)
        {
            throw new MissingResourceError("There are no meetings")
        }
        return meetings
    }

    async getMeetingByID(meeting_id:number): Promise<Meeting>
    {
        const sql:string = 'select * from meeting where meeting_id = $1';
        const values = [meeting_id]
        const result = await connection.query(sql,values);
        const row = result.rows[0];
        if(result.rowCount === 0)
        {
            throw new MissingResourceError(`The Meeting with id ${meeting_id} does not exist`)
        }
        const meeting: Meeting = new Meeting(row.meeting_id, row.meeting_location, row.meeting_time, row.meeting_topic)
        return meeting;
    }

    async deleteMeetingByID(meeting_id:number): Promise<boolean>
    {
        const sql:string = 'delete from meeting where meeting_id = $1'
        const values = [meeting_id]
        const result = await connection.query(sql,values);
        if(result.rowCount === 0)
        {
            throw new MissingResourceError(`The meeting with id ${meeting_id} does not exist`)
        }
        return true;
    }

    async createMeeting(meeting: Meeting): Promise<Meeting> {
        const sql:string = "insert into meeting(meeting_location, meeting_time, meeting_topic) values ($1,$2,$3) returning meeting_id";
        const values = [meeting.meeting_location, meeting.meeting_time, meeting.meeting_topic];
        const result = await connection.query(sql, values);
        meeting.meeting_id = result.rows[0].meeting_id;
        return meeting;
    }

   async updateMeetingByID(meeting: Meeting): Promise<Meeting> {
       const sql: string = 'update meeting set meeting_location =$1, meeting_time =$2, meeting_topic =$3 where meeting_id = $4';
       const values = [meeting.meeting_location, meeting.meeting_time, meeting.meeting_topic, meeting.meeting_id]
       const result = await connection.query(sql, values);
       console.log(this.updateMeetingByID)
       //meeting.meeting_ID = result.rows[0].meeting_ID;

       return meeting;
   } 
}