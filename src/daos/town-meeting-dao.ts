import { Meeting } from "../entities";

export interface TownMeetingDAO
{
    getAllMeetings(): Promise<Meeting[]>

    getMeetingByID(meeting_id:number): Promise<Meeting>

    deleteMeetingByID(meeting_id:number): Promise<boolean>

    createMeeting(meeting:Meeting):Promise<Meeting>

    updateMeetingByID(meeting_id:Meeting): Promise<Meeting>


}