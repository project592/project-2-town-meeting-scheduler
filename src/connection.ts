import { Client } from "pg";

export const connection = new Client({
    user: 'postgres',
    password: process.env.DBPASSWORD, // Should be process.env.DBPASSWORD
    database: 'townmeetingdb',
    port: 5432,
    host: '34.150.149.180'
})
//console.log(process.env.DBPASSWORD)
connection.connect()