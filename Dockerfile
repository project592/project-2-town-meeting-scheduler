FROM node
COPY . /workspace
WORKDIR /workspace
EXPOSE 3000
RUN npm install
ENTRYPOINT [ "npm","start" ]

# tried
# "node" "./dist/src/index.js" -- didnt work
# "node" "/workspace/dist/src/index.js" -- didnt work