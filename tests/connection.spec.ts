import {connection} from "../src/connection";

test("Should have a connection ", async() => {
    const result = await connection.query("select * from meeting");
    console.log(result);
})

afterAll(async() => {
    connection.end();
})